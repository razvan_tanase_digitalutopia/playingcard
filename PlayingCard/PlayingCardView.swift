//
//  PlayingCardView.swift
//  PlayingCard
//
//  Created by Razvan Tanase on 28/08/2019.
//  Copyright © 2019 Razvan Tanase. All rights reserved.
//

import UIKit

@IBDesignable
class PlayingCardView: UIView {
    
    @IBInspectable
    var rank: Int = 4 {didSet {setNeedsDisplay(); setNeedsLayout()}}
    @IBInspectable
    var suit: String = "❤️" {didSet {setNeedsDisplay(); setNeedsLayout()}}
    @IBInspectable
    var isFaceUp :Bool = true {didSet {setNeedsDisplay(); setNeedsLayout()}}
    
    private func centeredAttributedString(_ string: String, fontSize: CGFloat) -> NSAttributedString{
        var font = UIFont.preferredFont(forTextStyle: .body).withSize(fontSize)
        font = UIFontMetrics(forTextStyle: .body).scaledFont(for: font)
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = .center
        return NSAttributedString(string: string , attributes: [ .paragraphStyle: paragraphStyle, .font: font])
    }
    
    private var cornerString: NSAttributedString{
        return centeredAttributedString(rankString+"\n"+suit, fontSize: 0.0)
    }
    
    private lazy var upperLeftCornerLabel=createCornerLabel()
    private lazy var lowerRightCornerLabe=createCornerLabel()
    
    private func createCornerLabel() -> UILabel{
        let label = UILabel()
        label.numberOfLines = 0
        addSubview(label)
        return label
    }
    
    private func configureCorenerLabel(_ label: UILabel){
        label.attributedText = cornerString
        label.frame.size = CGSize.zero
        label.sizeToFit()
        label.isHidden = !isFaceUp
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        configureCorenerLabel(upperLeftCornerLabel)
        upperLeftCornerLabel.frame.origin = bounds.origin.offsetBy(dx: cornerOffset, dy: cornerOffset)
        
        configureCorenerLabel(lowerRightCornerLabe)
        lowerRightCornerLabe.transform = CGAffineTransform.identity
            .translatedBy(x: lowerRightCornerLabe.frame.size.width, y: lowerRightCornerLabe.frame.size.height)
            .rotated(by:  CGFloat.pi)
        lowerRightCornerLabe.frame.origin = CGPoint(x: bounds.maxX ,y: bounds.maxY).offsetBy(dx: -cornerOffset, dy: -cornerOffset)
            .offsetBy(dx: -lowerRightCornerLabe.frame.size.width, dy: -lowerRightCornerLabe.frame.size.height)
    }
    
     override func draw(_ rect: CGRect) {
        let roundedRect = UIBezierPath(roundedRect: bounds, cornerRadius: cornerFontSize)
        roundedRect.addClip()
        UIColor.white.setFill()
        roundedRect.fill()
     }
 

}

extension PlayingCardView {
    private struct SizeRatio{
        static let cornerFontSizeToBoundsHeight: CGFloat = 0.085
        static let cornerRadiusToBoundsHeight: CGFloat = 0.06
        static let cornerOffsetToCornerRadius: CGFloat = 0.33
        static let faceCardImageSizeToBoundsSize: CGFloat = 0.75
    }
    
    private var cornerRadius: CGFloat{
        return bounds.size.height * SizeRatio.cornerRadiusToBoundsHeight
    }
    
    
    private var cornerOffset: CGFloat{
        return cornerRadius * SizeRatio.cornerOffsetToCornerRadius
    }
    
    private var cornerFontSize: CGFloat{
        return bounds.size.height * SizeRatio.cornerFontSizeToBoundsHeight
    }
    
    private var rankString: String{
        switch rank {
        case 1: return "A"
        case 2...10: return String(rank)
        case 11: return "J"
        case 12: return "Q"
        case 13: return "K"
        default:
            return "?"
        }
    }
    
}

extension CGRect {
    var leftHalf: CGRect{
        return CGRect(x: minX, y: minY, width: width/2, height: height/2)
    }
    var rightHalf: CGRect{
        return CGRect(x: minX, y: minY, width: width/2, height: height/2)
    }
    func inset(by size: CGSize) -> CGRect{
        return insetBy(dx: size.width, dy: size.height)
    }
    func sized(by size: CGSize) -> CGRect{
        return CGRect(origin: origin, size: size)
    }
    func zoom(by scale:CGFloat)->CGRect{
        let newWidth = width * scale
        let newHeight = height * scale
        return insetBy(dx: (width-newWidth)/2, dy: (height-newHeight)/2)
    }
    
    
}

extension CGPoint{
    func offsetBy(dx: CGFloat, dy: CGFloat) -> CGPoint{
        return CGPoint(x: x+dx,y: y+dy)
    }
}
